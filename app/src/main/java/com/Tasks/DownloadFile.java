package com.Tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.Models.Trie;
import com.example.android.softkeyboard.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFile extends AsyncTask<String, Void, String>{
    private File cache_file, dictionary_file;

    public DownloadFile (Context context){
        String cache_file_name = context.getResources().getString(R.string.word_frequency_list);
        cache_file = new File(context.getCacheDir(), cache_file_name);

        String dictionary_file_name = context.getResources().getString(R.string.dictionary);
        dictionary_file = new File(context.getCacheDir(), dictionary_file_name);
    }

    @Override
    protected String doInBackground(String... params) {
        InputStream input = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // download the file
            input = connection.getInputStream();

            saveFileToCache(input);
            createDictionary();
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    private void saveFileToCache(InputStream input) throws IOException {
        FileOutputStream output = new FileOutputStream(cache_file);
        byte data[] = new byte[4096];
        int count;
        while ((count = input.read(data)) != -1) {
            output.write(data, 0, count);
        }
        output.close();
    }

    private void createDictionary() throws IOException {
        Trie dictionary = new Trie();
        FileInputStream fis = new FileInputStream(cache_file);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader bufferedReader = new BufferedReader(isr);
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String [] words_frequency = line.split(" ");
            dictionary.insert(words_frequency[0], Integer.valueOf(words_frequency[1]));
        }
        saveDictionary(dictionary);
        fis.close();
        isr.close();
    }

    private void saveDictionary(Trie dictionary){
        try {
            // Use a FileOutputStream to send data to a file called myobject.data.
            FileOutputStream f_out = new FileOutputStream(dictionary_file);

            // Use an ObjectOutputStream to send object data to the FileOutputStream for writing to disk.
            ObjectOutputStream obj_out = new ObjectOutputStream (f_out);

            // Pass our object to the ObjectOutputStream's writeObject()
            // method to cause it to be written out to disk.
            obj_out.writeObject (dictionary);

            f_out.close();
            obj_out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}