package com.Models;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trie implements java.io.Serializable{
    private TrieNode root;

    // Constructor
    public Trie() {
        root = new TrieNode((char)0);
    }

    // Method to insert a new word to Trie
    public void insert(String word, int frequency)  {

        // Find length of the given word
        int length = word.length();
        TrieNode crawl = root;

        // Traverse through all characters of given word
        for( int level = 0; level < length; level++) {
            HashMap<Character,TrieNode> child = crawl.getChildren();
            char ch = word.charAt(level);

            // If there is already a child for current character of given word
            if( child.containsKey(ch)){
                crawl = child.get(ch);
            } else  {  // Else create a child
                TrieNode temp = new TrieNode(ch);
                child.put( ch, temp );
                crawl = temp;
            }
        }

        // Set bIsEnd true for last character
        crawl.setIsEnd(true);
        crawl.setWord(word);
        crawl.setFrequency(frequency);
    }

    public List<String> autoComplete(String prefix) {
        // Initialize reference to traverse through Trie
        TrieNode crawl = root;
        int length = prefix.length();  // Find length of the input string

        // Iterate through all characters of input string 'str' and traverse
        // down the Trie
        for( int level = 0; level < length; level++) {
            HashMap<Character,TrieNode> child = crawl.getChildren();
            char ch = prefix.charAt(level);

            // If there is already a child for current character of given word
            if( !child.containsKey(ch)){
                return null;
            }
            crawl = child.get(ch);
        }

        List<Word> words = new ArrayList<Word>();
        Deque<TrieNode> DQ = new ArrayDeque<TrieNode>();
        DQ.addLast(crawl);
        while (!DQ.isEmpty()) {
            TrieNode first = DQ.removeFirst();
            if(first.isEnd()){
                Word word = new Word();
                word.setWord(first.getWord());
                word.setFrequency(first.getFrequency());
                words.add(word);
            }

            for(char currentKey : first.getChildren().keySet()){
                if(first.getChildren().get(currentKey) != null){
                    DQ.add(first.getChildren().get(currentKey));
                }
            }
        }

        heapSort(words, 5);

        for (int i = 0; i < 5; i++){
            Word word = words.get(i);
        }
        return null;
    }

    public static void heapSort(List<Word> words, int array_size){
        int i;
        Word temp;

        for (i = (array_size / 2)-1; i >= 0; i--)
            siftDown(words, i, array_size);

        for (i = array_size-1; i >= 1; i--)
        {
            temp = words.get(0);
            words.remove(0);
            words.add(0,words.get(i));

            words.remove(i);
            words.add(i, temp);
            siftDown(words, 0, i-1);
        }
    }

    public static void siftDown(List<Word> words, int root, int bottom) {
        int maxChild;

        Word temp;
        boolean done = false;
        while ((root*2 <= bottom) && (!done))
        {
            if (root*2 == bottom)
                maxChild = root * 2;
            else if (words.get(root * 2).getFrequency() > words.get(root * 2 + 1).getFrequency())
                maxChild = root * 2;
            else
                maxChild = root * 2 + 1;

            if (words.get(root).getFrequency() < words.get(maxChild).getFrequency())
            {
                temp = words.get(root);

                words.remove(root);
                words.add(root, words.get(maxChild));

                words.remove(maxChild);
                words.add(maxChild, temp);
                root = maxChild;
            }
            else
                done = true;
        }
    }
}
