package com.Models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Trie Node, which stores a character and the children in a HashMap
class TrieNode implements java.io.Serializable {
    private char value;
    private int frequency = 0;
    private HashMap<Character, TrieNode> children;
    private boolean bIsEnd;
    private String word = "";

    TrieNode(char ch) {
        value = ch;
        children = new HashMap<>();
        bIsEnd = false;
    }

    public HashMap<Character, TrieNode> getChildren() {
        return children;
    }

    public void setValue(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }

    public void setIsEnd(boolean val) {
        bIsEnd = val;
    }

    public boolean isEnd() {
        return bIsEnd;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getFrequency() {
        return frequency;
    }

    protected ArrayList<String> allPrefixes() {
        ArrayList<String> results = new ArrayList<String>();
        if (this.bIsEnd) {
            results.add(String.valueOf(this.value));
        }
        for (Map.Entry<Character, TrieNode> entry : children.entrySet()) {
            TrieNode child = entry.getValue();
            Collection<String> childPrefixes = child.allPrefixes();
            results.addAll(childPrefixes);
        }
        return results;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}